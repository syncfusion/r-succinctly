# R Succinctly
# sample_2_1.R
# Creating bar charts for categorical variables

# LOAD DATA
require("datasets")  # Loads datasets package

# DEFAULT CHART WITH PLOT()
plot(chickwts$feed)  # Default method to plot the variable "feed"

# CREATE TABLE
feeds <- table(chickwts$feed)  # Create a table of feed, place in “feeds”
feeds  # See contents of object “feeds”
barplot(feeds)  # Identical to plot(chickwts$feed) but with new object

# USE BARPLOT() AND PAR() FOR PARAMETERS
oldpar <- par()  # Stores current graphical parameters
par(oma = c(1, 4, 1, 1))  # Sets outside margins: bottom, left, top, right
par(mar = c(4, 5, 2, 1))  # Sets plot margins
barplot(feeds[order(feeds)],  # Order the bars by descending values
        horiz  = TRUE,  # Make the bars horizontal
        las    = 1,  # las gives orientation of axis labels
        col    = c("beige", "blanchedalmond", "bisque1", "bisque2",
                   "bisque3", "bisque4"),  # Vector of colors for bars
        border = NA,  # No borders on bars
        # Add main title and label for x axis
        main   = "Frequencies of Different Feeds in chickwts Dataset",
        xlab   = "Number of Chicks")  

# CLEAN UP
par(oldpar)  # Restores previous parameters (ignore errors)
detach("package:datasets", unload = TRUE)  # Unloads datasets package
rm(list = ls())  # Remove all objects from workspace
