# R Succinctly
# sample_6_2.R
# Creating bar charts of group means

# LOAD DATA
require("datasets")  # Load datasets package
spray <- InsectSprays  # Load data with shorter name

# GET GROUP MEANS
means <- aggregate(spray$count ~ spray$spray, FUN = mean)
means  # Check data

# REORGANIZE DATA FOR BARPLOT
mean.data <- t(means[-1])  # Removes first column, transposes second
colnames(mean.data) <- means[, 1]  # Add group names as col. names
mean.data

# BARPLOT WITH DEFAULTS
barplot(mean.data)

# BARPLOT WITH OPTIONS
barplot(mean.data,
        col  = "red",
        main = "Effectiveness of Insect Sprays",
        xlab = "Spray Used",
        ylab = "Insect Count")

# CLEAN UP
detach("package:datasets", unload = TRUE)  # Unloads datasets package
rm(list = ls())  # Remove all objects from workspace
