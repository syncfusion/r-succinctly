# R Succinctly
# sample_9_2.R
# Two-factor ANOVA

# LOAD DATA
require("datasets")  # Load datasets package
data(warpbreaks)

# ANOVA: METHOD 1
aov1 <- aov(breaks ~ wool + tension + wool:tension, 
            data = warpbreaks)
summary(aov1)  # ANOVA table

# ANOVA: METHOD 2
aov2 <- aov(breaks ~ wool*tension, 
            data = warpbreaks)

# CLEAN UP
detach("package:datasets", unload = TRUE)  # Unloads datasets package
rm(list = ls())  # Remove all objects from workspace