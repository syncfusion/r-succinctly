# R Succinctly
# sample_3_3.R
# Single proportion: Hypothesis test and confidence interval

# PROPORTIONS TEST WITH DEFAULTS
prop.test(27, 40)  # 27 heads in 40 coin flips

# PROPORTIONS TEST WITH OPTIONS
# One-tailed test with 90% CI
prop.test(27, 40,  # Same observed values
          p = .6,  # Null probability of .6 (vs. .5)
          alt = "greater",  # Directional test for greater value
          conf.level = .90)  # Confidence leve of 90% (vs. 95%)

# CLEAN UP
rm(list = ls())  # Remove all objects from workspace