# R Succinctly
# sample_5_3.R
# Merging files

# LOAD DATA
require("datasets")  # Load datasets package

# DISPLAY DATA
longley[1:3, ]  # Display first three rows, all variables

# SPLIT & EXPORT DATA
a1 <- longley[1:14, 1:6]  # First 14 cases, first 6 variables
a2 <- longley[1:14, 6:7]  # First 14 cases, last 2 variables
b <- longley[15:16, ]     # Last 2 cases, all variables
write.table(a1, "~/Desktop/longley.a1.txt", sep = "\t")
write.table(a2, "~/Desktop/longley.a2.txt", sep = "\t")
write.table(b, "~/Desktop/longley.b.txt", sep = "\t")
# On PC, use "c:/longley.a1.txt"
rm(list=ls()) # Clear out everything to start fresh

# IMPORT & COMBINE FIRST TWO DATASETS
# Add columns for same cases
a1t <- read.table("~/Desktop/longley.a1.txt", sep = "\t")
a2t <- read.table("~/Desktop/longley.a2.txt", sep = "\t")
# Take early years (a1t) and add columns (a2t)
# Must specify variable to match cases ("Year" in this case)
a.1.2 <- merge(a1t, a2t, by = "Year")  # Merge two data frames
a.1.2[1:3, ]  # Check results for first three cases

# IMPORT & COMBINE LAST DATASET
# Add two more cases at bottom
b <- read.table("~/Desktop/longley.b.txt", sep = "\t")
all.data <- rbind(a.1.2, b)  # "Row Bind"
all.data[13:16, ]  # Check last four rows, all variables

# CLEAN DATA
row.names(all.data) <- NULL  # Reset row names
all.data[13:16, ]  # Check last four rows, all variables

# CLEAN UP
detach("package:datasets", unload = TRUE)  # Unloads datasets package
rm(list = ls())  # Remove all objects from workspace