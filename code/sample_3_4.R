# R Succinctly
# sample_3_4.R
# Single mean: Hypothesis test and confidence interval

# LOAD DATA & EXAMINE
require("datasets")  # Loads datasets package
mag <- quakes$mag  # Just load the magnitude variable
hist(mag)
summary(mag)

# T-TEST WITH DEFAULTS
t.test(mag)

# T-TEST WITH OPTIONS
t.test(mag,
       alternative = "greater",  # Directional test
       mu = 4.5,                 # Null population mean of 4.5
       conf.level = 0.99)        # Confidence level of 99%

# CLEAN UP
detach("package:datasets", unload = TRUE)  # Unloads datasets package
rm(list = ls())  # Removes all objects from workspace