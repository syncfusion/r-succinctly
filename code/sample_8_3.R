# R Succinctly
# sample_8_3.R
# Scatterplot matrices

# LOAD DATA
require("datasets")  # Load datasets package
data(iris)  # Load data into workspace
iris[1:3, ]  # Show first three lines of data

# SCATTERPLOT MATRIX WITH DEFAULTS
pairs(iris[1:4])

# FUNCTION TO PUT HISTOGRAMS ON DIAGONAL
# Adapted from code in "pairs" help
panel.hist <- function(x, ...)
{
  usr <- par("usr")  # Copies usr parameters for plot coordinates
  on.exit(par(usr))  # Restores parameters on exit
  par(usr = c(usr[1:2], 0, 1.5) )  # Sets plot coordinates
  h <- hist(x, plot = FALSE)  # Creates histogram
  breaks <- h$breaks  # Reads breaks for histograms
  nB <- length(breaks)  # Reads number of breaks
  y <- h$counts  # Get raw values for Y axis
  y <- y/max(y)  # Adjusts raw values to fit Y scale
  rect(breaks[-nB], 0, breaks[-1], y,  ...)  # Draws boxes
}

# SET COLOR PALETTE WITH RCOLORBREWER
require("RColorBrewer")
display.brewer.pal(3, "Pastel1")

# SCATTERPLOT MATRIX WITH OPTIONS
pairs(iris[1:4],  # Reads data
      panel = panel.smooth,  # Adds optional smoother
      main = "Scatterplot Matrix for Iris Data Using pairs Function",
      diag.panel = panel.hist,  # Calls histogram function
      pch = 16,  # Uses solid dots for point
      # Next line color dots by "Species" category
      col = brewer.pal(3, "Pastel1")[unclass(iris$Species)])

# SCATTERPLOT MATRIX WITH "CAR" PACKAGE
require("car")
scatterplotMatrix(~Petal.Length + Petal.Width + 
                   Sepal.Length + Sepal.Width | Species,
                  data = iris,
                  main = paste("Scatterplot Matrix for Iris Data",
                               "Using the \"car\" Package"))

# CLEAN UP
palette("default")  # Return to default
detach("package:datasets", unload = TRUE)  # Unloads datasets package
detach("package:RColorBrewer", unload = TRUE)  # Unloads RColorBrewer
detach("package:car", unload=TRUE)  # Unloads car
rm(list = ls())  # Remove all objects from workspace
