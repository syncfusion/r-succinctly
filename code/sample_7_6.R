# R Succinctly
# sample_7_6.R
# Comparing proportions

# CREATE SIMULATION DATA FOR 5 GROUPS
# Need two vectors:
# One specifies the total number of people in each group
# This creates a vector with 5 100s in it, for 5 groups
n5 <- c(rep(100, 5))
# Another specifies the number of cases with positive outcome 
x5 <- c(65, 60, 60, 50, 45)

# PROPORTION TEST FOR 5 GROUPS
prop.test(x5, n5)

# CREATE SIMULATION DATA FOR 2 GROUPS
n2 <- c(40, 40)  # 40 trials
x2 <- c(30, 20)  # Number of positive outcomes

# PROPORTION TEST FOR 2 GROUPS
prop.test(x2, n2, conf.level = .80)  # With CI

# CLEAN UP
rm(list = ls())  # Remove all objects from workspace
